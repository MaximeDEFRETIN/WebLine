$(function () {
    $('#listeProjets > tr').each(function(index) {
        if (index > 4) {
            $(this).attr('hidden', 'hidden');
        }
    
         if ($('#moreProjets').click(function() {
                     $('#listeProjets > tr').each(function(index) {
                if (index > 4) {
                    $(this).removeAttr('hidden', 'hidden');
                }
            });
         }));
    });
    
    $('#listeOpinionsVa > tr').each(function(index) {
        if (index > 4) {
            $(this).attr('hidden', 'hidden');
        }
    
         if ($('#moreOpinionsVa').click(function() {
                     $('#listeOpinionsVa > tr').each(function(index) {
                if (index > 4) {
                    $(this).removeAttr('hidden', 'hidden');
                }
            });
         }));
    });
    
    $('#listeOpinionsNo > tr').each(function(index) {
        if (index > 4) {
            $(this).attr('hidden', 'hidden');
        };
        
        if ($('#moreOpinionsNo').click(function() {
            $('#listeOpinionsNo > tr').each(function(index) {
                if (index > 4) {
                    $(this).removeAttr('hidden', 'hidden');
                };
            });
         }));
    });
    
    $('.modal').modal();
    $('#title, #link, #project_owner, #description_project').characterCounter();
});