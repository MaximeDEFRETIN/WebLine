$(function() {
    $('.modal').modal();
    $('#mail, #author, #opinion, #title').characterCounter();
    $('.carousel').carousel();
    $('.parallax').parallax();
    $('.scrollspy').scrollSpy();
    
    function displayNumber(id, number, vitesse, signe) {
        var i=0;
        var timer = setInterval(function(){
            $("#"+id).replaceWith('<span id="'+id+'" class="valueStats">'+i+++signe+'</span>');
            if(i>number){
                clearInterval(timer);
            }
        }, vitesse);
    }

    displayNumber('numSearch', 83, 50, '%');
    displayNumber('numStatEC', 2304, 5, '$');
    
    $widthWindow = $(document).width();
    if ($widthWindow <= 490) {
        console.log($widthWindow);
        $('footer').addClass('centerV');
    }
    
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-128768077-1');
  
  $('#mailCo, #sujet, #prenom').characterCounter();
  
    $('#contact').submit(function(event){
        var prenom = $('#prenom').val();
        var text = $('#text').val();
        var mail = $('#mail').val();
        var sujet = $('#sujet').val();
        var dataString = prenom + text + mail + sujet;
        var msg_alert = 'Merci de remplir ce champ.';

        if (dataString  == '') {
            $('#msg_all').html('Merci de donner toutes les informtions nescessaires.');
        } else if (prenom == '') {
            $('#msg_prenom').html(msg_alert);
        } else if (sujet == '') {
            $('#msg_sujet').html(msg_alert);
        } else if (mail == '') {
            $('#msg_mail').html(msg_alert);
        } else if (text == '') {
            $('#msg_text').html(msg_alert);
        } else {
            $.ajax({
                type : 'POST',
                url: 'controllers/frontOffice/contact.php',
                data: $(this).serialize(),
                success : function() {
                    $('#contact').html('<p>Ton message a bien été envoyé.</p>');
                },
                error: function() {
                    $('#contact').html('<p>Il y a eu un problème avec le formulaire.</p>');
                }
            });
        }
        return false;
    });
});