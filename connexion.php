<?php
require_once 'header.php';
require_once 'controllers/frontOffice/connexion.php';
?>
<div class="container marginTopMax">
    <h1 class="center-align">Connexion !</h1>
    <form class="" method="POST">
        <div class="input-field center-align col s12">
            <input class="validate" id="mailConnection" type="email" name="mailConnection" required />
            <label for="mailConnection" data-error="Adresse mail faussement écris." data-success="Adresse mail correctement écris." class="black-text">Adresse mail</label>
        </div>
        <div class="input-field center-align col s12">
            <input class="validate" type="password" id="passwordConnection" name="passwordConnection" required />
            <label for="passwordConnection" class="black-text">Mot de passe</label>
        </div>
        <input class="btn col s4 offset-s4" name="submit" type="submit" />
    </form>
</div>
<?php foreach ($connectionMessage as $message) { ?>
    <p><?= $message ?></p>
<?php } ?>
<?php require_once 'footer.php'; ?>