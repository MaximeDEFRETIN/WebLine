<?php
require_once 'models/dataBase.php';
require_once 'models/user.php';
require_once 'models/projets.php';
require_once 'models/opinions.php';
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device=width, initial-scale=1.0" />
        <meta name="keywords" content="Projets, À propos, projet, SEO, WebLine, Créateur de site web pour professionnels, associations et particuliers." />
        <meta name="google-site-verification" content="GXflSFX9AKz9Jxx6dmYyLwMwPIP1aNQrUVm26UTvQ3w" />
        <meta name="robots" content="index" />
        <meta name="description" content="Créateur de site web pour professionnels, associations et particuliers." />
        <meta property="og:title" content=WebLine />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://w-l.fr/" />
        <meta property="og:description" content="Créateur de site web pour professionnels, associations et particuliers." />
        <meta property="og:locale" content="fr_FR" />
        <meta property="og:site_name" content="WebLine" />
        <link rel="stylesheet" href="../bower_components/materialize/dist/css/materialize.min.css" />
        <link rel="stylesheet" href="../asset/frontOffice/css/style.min.css" />
        <title>WebLine</title>
    </head>
    <body>