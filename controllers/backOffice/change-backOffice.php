<?php
if (isset($_GET['chPr']) && isset($_SESSION['id'])) {
    if (filter_var($_GET['chPr'], FILTER_VALIDATE_INT)) {
        $projetBackChange = new project_displayed();
        $projetBackOfficeChange = $projetBackChange->getOneProject($_GET['chPr']);
    } else {
        header('Location: index.php');
    }
}


$messageUpdateProject = array();
if (isset($_POST['submitUp'])) {
    $updateProject = new project_displayed();
    $regexUpdate = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

    if (empty($_POST['titleUp']) && empty($_POST['descriptionUp']) && empty($_POST['propretaireUp']) && empty($_POST['linkUp'])) {
        $messageUpdateProject['emptyForm'] = 'Le formulaire est vide.';
    } else {
        if (empty($_POST['titleUp'])) {
            $messageUpdateProject['emptyTitle'] = 'Il n\'y a pas de titre.';
        } else {
            (preg_match($regexUpdate, $_POST['titleUp']))?$title=htmlspecialchars(strip_tags($_POST['titleUp'])):'';
        }
        
        if (empty($_POST['descriptionUp'])) {
            $messageUpdateProject['emptyDescription'] = 'Il n\'y a pas de description.';
        } else {
            (preg_match($regexUpdate, $_POST['descriptionUp']))?$description_project=htmlspecialchars(strip_tags($_POST['descriptionUp'])):'';
        }
        
        if (empty($_POST['propretaireUp'])) {
            $messageUpdateProject['emptyPropriétaire'] = 'Il n\'y a pas le nom du proprtiétaire.';
        } else {
            (preg_match($regexUpdate, $_POST['propretaireUp']))?htmlspecialchars(strip_tags($project_owner=$_POST['propretaireUp'])):'';
        }
        
        if (empty($_POST['linkUp'])) {
            $messageUpdateProject['emptyLink'] = 'Il n\'y a pas de lien.';
        } else {
            (filter_var($_POST['linkUp'], FILTER_VALIDATE_URL))?$link=$_POST['linkUp']:'';
        }
       
       (filter_var($_GET['chPr'], FILTER_VALIDATE_INT))?$id=$_GET['chPr']:$messageUpdateProject['idNoValid'] = 'Ce n\'est pas le bon projet.';
        
        // On vérifie que le fichier bien été envoyé et qu'il n'y a pas d'erreur
        if (!empty($_FILES['chimg']['name']) && $_FILES['chimg']['error'] == 0) {
                // On vérifie que e fichier ne dépasse pas une limite autorisée
                if ($_FILES['chimg']['size'] <= 5000000) {
                    // On récupère les infos sur le fichier
                    $fileType = pathinfo($_FILES['chimg']['name']);
                    // On vérifie que le fichier ait bien un format et un content type attendu
                    $extension_upload = $fileType['extension'];
                    $extensions_autorisees = array('jpeg');
                    $contentType = $_FILES['chimg']['type'];
                    $contentType_autorisees = array('image/jpeg');
                    if (in_array($extension_upload, $extensions_autorisees)) {
                        if (in_array($contentType, $contentType_autorisees)) {
                            unlink('/var/www/WebLine/asset/global/img/projets/' . $_POST['pi1']);
                            // On déplace le fichier
                            move_uploaded_file($_FILES['chimg']['tmp_name'], '/var/www/WebLine/asset/global/img/projets/' . $_FILES['chimg']['name']);
                            // On change ses droits
                            chmod('/var/www/WebLine/asset/img/projets/' . $_FILES['chimg']['name'], 0660);
                        } else {
                            $messageUpdateProject['contentFile'] = 'Le contenu du fichier n\'est pas autorisée.';
                        }
                    } else {
                        $messageUpdateProject['extensionFile'] = 'L\'extension du fichier n\'est pas autorisée.';
                    }
                } else {
                    $messageUpdateProject['sizeFile'] = 'Le fichier est trop lourd.';
                }
        } else {
            $messageUpdateProject['emptyFile'] = 'Il faut envoyer une image au format JPEG.';
        }
    
       $updateProject->updateProject($_SESSION['id'], $title, $description_project, $project_owner, $link, $id, $_FILES['chimg']['name']);
       $messageUpdateProject['update'] = 'Projet mis à jour.';
       header('refresh: 2; url=../Interface');
    }
}