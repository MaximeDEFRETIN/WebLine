<?php
$projetsBackOffice = new project_displayed();
$projetsBackOfficeDisplay = $projetsBackOffice->getProjects();

if(isset($_SESSION['id'])) {
    if (isset($_GET['delPr'])) {
        $delProject = new project_displayed();
        unlink('/var/www/WebLine/asset/global/img/projets/'.$_GET['path']);
        
        if(filter_var($_GET['delPr'], FILTER_VALIDATE_INT)) {
            $delProject->deleteProject($_GET['delPr']);
            header('Location: ../Interface');
        }
    }
}

$messageInsert = array();
if (isset($_POST['projectSubmit'])) {
    $requestInsert = new project_displayed();
    $regexInsert = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

    if (empty($_POST['title']) && empty($_POST['link']) && empty($_POST['project_owner']) && empty($_POST['description_project'])) {
        $messageInsert['emptyForm'] = 'Le formulaire est vide.';
    } else {
        if (empty($_POST['title'])) {
            $messageInsert['emptyTitle'] = 'Il n\'y a pas de titre.';
        } else {
            (preg_match($regexInsert, $_POST['title'])) ? $title = htmlspecialchars(strip_tags($_POST['title'])) : $messageInsert['wrongTitle'] = 'Le titre n\'est pas écris correctement.';
        }

        if (empty($_POST['link'])) {
            $messageInsert['emptyLink'] = 'Il n\'y a pas de lien.';
        } else {
            (filter_var($_POST['link'], FILTER_VALIDATE_URL)) ? $link = $_POST['link'] : $messageInsert['wrongLink'] = 'Ce n\'est pas un lien.';
        }

        if (empty($_POST['project_owner'])) {
            $messageInsert['emptyPrOwn'] = 'Il n\'y a pas de propriétaire.';
        } else {
             (preg_match($regexInsert, $_POST['project_owner'])) ? $project_owner = $_POST['project_owner'] : $messageInsert['wrongPrOwn'] = 'Le nom n\'est pas écris correctement.';
        }

        if (empty($_POST['description_project'])) {
            $messageInsert['emptyDescription'] = 'Il n\'y a pas de description.';
        } else {
             (preg_match($regexInsert, $_POST['description_project'])) ? $description_project = $_POST['description_project'] : $messageInsert['wrongDesOw'] = 'La description n\'est pas écris correctement.';
        }
        
        // On vérifie que le fichier bien été envoyé et qu'il n'y a pas d'erreur
        if (!empty($_FILES['img']['name']) && $_FILES['img']['error'] == 0) {
                // On vérifie que e fichier ne dépasse pas une limite autorisée
                if ($_FILES['img']['size'] <= 5000000) {
                    // On récupère les infos sur le fichier
                    $fileType = pathinfo($_FILES['img']['name']);
                    // On vérifie que le fichier ait bien un format et un content type attendu
                    $extension_upload = $fileType['extension'];
                    $extensions_autorisees = array('jpeg');
                    $contentType = $_FILES['img']['type'];
                    $contentType_autorisees = array('image/jpeg');
                    if (in_array($extension_upload, $extensions_autorisees)) {
                        if (in_array($contentType, $contentType_autorisees)) {
                            // On déplace le fichier
                            move_uploaded_file($_FILES['img']['tmp_name'], '/var/www/WebLine/asset/global/img/projets/' . $_FILES['img']['name']);
                            // On change ses droits
                            chmod('/var/www/WebLine/asset/global/img/projets/' . $_FILES['img']['name'], 0660);
                            header('refresh: 2; url=../Interface');
                        } else {
                            $messageInsert['contentFile'] = 'Le contenu du fichier n\'est pas autorisée.';
                        }
                    } else {
                        $messageInsert['extensionFile'] = 'L\'extension du fichier n\'est pas autorisée.';
                    }
                } else {
                    $messageInsert['sizeFile'] = 'Le fichier est trop lourd.';
                }
        } else {
            $messageInsert['emptyFile'] = 'Il faut envoyer une image au format JPEG.';
        }

        if(count($messageInsert) == 0) {
            $requestInsert->insertProject($title, $description_project, $project_owner, $link, $_FILES['img']['name']);
            $messageInsert['insert'] = 'Projet ajouté.';
            header('refresh: 50; url=../Interface');
        } else {
            $messageInsert['error'] = 'Il y a une erreur.';
            header('refresh: 2; url=../Interface');
        }
    }
}

$opinionBackOffice = new opinion();
$opinionBackOfficeDisplay = $opinionBackOffice->getOpinion(1, 'DESC');
$opinionBackOfficeDisplayNo = $opinionBackOffice->getOpinion(0, 'DESC');

if(isset($_SESSION['id'])) {
    if (isset($_GET['vaOp']) && filter_var($_GET['vaOp'], FILTER_VALIDATE_INT)) {
        
        $upOpinion->updateOpinion(1, $_GET['vaOp']);
            
        header('Location: ../Interface');
    }
}

if(isset($_SESSION['id'])) {
    if (isset($_GET['delOp']) && filter_var($_GET['delOp'], FILTER_VALIDATE_INT)) {
        $delOpinion = new opinion();
        
        $delOpinion->deleteOpinion($_GET['delOp']);
        header('Location: ../Interface');
    }
}