<?php
// On intialise un objet
$lastProjet = new project_displayed();
$Projet = $lastProjet->getLastProject();

$projets = new project_displayed();
$projetsDisplay = $projets->getProjects();

if (isset($_GET['proj'])) {
     if (filter_var($_GET['proj'], FILTER_VALIDATE_INT)) {
        $projet = new project_displayed();
        $projetDisplay = $projet->getOneProject($_GET['proj']);
     } else {
         header('Location: projets.php');
     }
}

$opinionFrontOffice = new opinion();
$opinionFrontOfficeDisplay = $opinionFrontOffice->getOpinion(1, 'DESC');

$messageInsertOpinion = array();
if (isset($_POST['opinionSubmit'])) {
    $opinionSend = new opinion();
    $regexInsert = '/[A-ZÉÈÀÊÀÙÎÏÜËa-zéèàêâùïüë0-9.\-\'~$£%*#{}()`ç+=œ“€\/:;,!]+/';

    if (empty($_POST['author']) && empty($_POST['note']) && empty($_POST['opinion'])) {
        $messageInsertOpinion['emptyForm'] = 'Le formulaire est vide.';
    } else {
        if (empty($_POST['author'])) {
            $messageInsertOpinion['emptyAuthor'] = 'Tu n\'as pas donné ton nom.';
        } else {
            (preg_match($regexInsert, $_POST['author'])) ? $author = htmlspecialchars(strip_tags($_POST['author'])) : $messageInsertOpinion['wrongAuthor'] = 'Il y a un caractère inattendu dans le nom que tu as .';
        }

        if (empty($_POST['note'])) {
            $messageInsertOpinion['emptyNote'] = 'Tu n\'as pas donné de note.';
        } else {
            (filter_var($_POST['note'], FILTER_VALIDATE_INT)) ? $note = htmlspecialchars(strip_tags($_POST['note'])) : $messageInsertOpinion['wrongNote'] = 'La note que tu as donné ne correspond à un chiffre.';
        }

        if (empty($_POST['opinion'])) {
            $messageInsertOpinion['emptyOpinion'] = 'Tu n\'as pas donné ton avis.';
        } else {
            (preg_match($regexInsert, $_POST['opinion'])) ? $opinion = htmlspecialchars(strip_tags($_POST['opinion'])) : $messageInsertOpinion['wrongOpinion'] = 'Il y a un caractère inattendu dans l\'opinion que tu as donné.';
        }

        if(count($messageInsertOpinion) == 0) {
            $opinionSend->insertOpinion($author, $opinion, $note);
        
            $messageInsertOpinion['successInsert'] = 'Ton avis a été enregistré. Il apparaitra si il est validé.';
            header('refresh: 8; url=/');
        } else {
            $messageInsertOpinion['errorInsert'] = 'Il y a eu une erreur dans l\'enregistrement de ton avis.';
            header('refresh: 8; url=/');
        }
    }
}