<?php
ob_start();

$userConnection = new user();
$connectionMessage = array();

if (isset($_POST['submit'])) {
    if (!empty($_POST['mailConnection']) && !empty($_POST['passwordConnection'])) {
        (!empty($_POST['mailConnection']) && filter_var($_POST['mailConnection'], FILTER_VALIDATE_EMAIL))?$userConnection->getUserByMail($_POST['mailConnection']):$connectionMessage['emptyMail'] = 'Tu n\'as pas entré ton adresse mail.';

        if (!empty($_POST['passwordConnection'])) {
            (password_verify($_POST['passwordConnection'], $userConnection->password))?:$connectionMessage['wrongPassword'] = 'Le mot de passe que tu as donné n\'est pas correcte.';
        } else if (empty($_POST['passwordConnection'])){
            $connectionMessage['emptyPassword'] = 'Il faut donner un mot de passe.';
        }
        
        if (count($connectionMessage) == 0) {
            session_start();
            
            $_SESSION['id'] = $userConnection->id;
            $_SESSION['last_name'] = $userConnection->last_name;
            $_SESSION['first_name'] = $userConnection->first_name;
            $_SESSION['mail'] = $userConnection->mail;
            $_SESSION['location'] = $userConnection->location;
            $_SESSION['username'] = $userConnection->username;
            $_SESSION['number_phone'] = $userConnection->number_phone;
            $_SESSION['birthdate_at'] = $userConnection->birthdate_at;
            $_SESSION['description'] = $userConnection->description;
            
            header('Location: ../Interface');
            exit;
        }
    } else {
        $connectionMessage['emptyForm'] = 'Il faut que tu remplisse le formulaire pour te connecter.';
    }
}

ob_end_flush();