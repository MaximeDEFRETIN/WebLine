<?php
    // CONDITIONS NOM
    if (!empty($_POST['prenom'])) {
        $prenom = htmlspecialchars(strip_tags($_POST['prenom']));
    } else {
        echo 'Merci d\'écrire un nom <br />';
        $prenom = '';
    }

    // CONDITIONS SUJET
    if (!empty($_POST['sujet'])) {
        $sujet = htmlspecialchars(strip_tags($_POST['sujet']));
    } else {
        echo 'Merci d\'écrire un sujet <br />';
        $sujet = '';
        var_dump($sujet);
    }

    // CONDITIONS EMAIL
    if (!empty($_POST['mail']) && filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
        $mail = htmlspecialchars(strip_tags($_POST['mail']));
    } elseif (!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
        echo 'Email invalide';
        $mail = '';
    } else {
        echo 'Merci d\'écrire une adresse email <br />';
        $mail = '';
    }

    // CONDITIONS MESSAGE
    if (!empty($_POST['text'])) {
        $text = htmlspecialchars(strip_tags($_POST['text']));
    } else {
        echo 'Merci d\'écrire un text<br />';
        $text = '';
    }

    // Les texts d'erreurs ci-dessus s'afficheront si Javascript est désactivé

    // PREPARATION DES DONNEES
    $objet        = $sujet . "\r\n";
    $contenu      = 'Nom de l\'expéditeur : ' . $prenom . "\r\n";
    $contenu     .= $text . "\r\n";
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=utf-8; DelSp=\"Yes\"; format=flowed" . "\r\n";
    $headers .= "Content-Disposition: inline" . "\r\n";
    $headers .= "Content-Transfer-Encoding: 7bit" . "\r\n";
    $headers .= "From: Maxime <maxime-defretin@w-l.fr>" . "\r\n";

    
    // ENCAPSULATION DES DONNEES 
    mail('maxime-defretin@w-l.fr', $objet, $contenu, $headers);
    echo 'Formulaire envoyé';