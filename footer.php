        <footer class="col s12 center-align valign-wrapper marginTopMax">
            <p class="waves-effect waves-light btn-flat modal-trigger" href="#mLe" title="Mentions légales">Mentions légales</p>
            <div id="mLe" class="modal">
                <div class="center-align">
                    <h4>Mentions légales</h4>
                    <p>Aucunes informations personnelles n'est stockées par WebLine.</p>
                    <p>Le site w-l.fr est hébergé par OVH, dont le siège sociale se situe 2 rue Kellermann à Roubaix (59100).</p>
                    <p><em>N° de SIRET</em> <em>842 317 943 00012</em></p>
                    <h5>Contact</h5>
                        <div>
                            <adress title="Numéro de téléphone grâce auquel vous pouvez me contacter">06 35 46 73 59</adress>
                        </div>
                        <div>
                            <adress title="Adresse mail grâce à laquelle vous pouvez me contacter">maxime-defretin@w-l.fr</adress>
                        </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" title="Fermer">D'accord</a>
                </div>
            </div>
            <p class="centerV waves-effect waves-light modal-trigger" href="#formContact" title="Vous souhaitez me contacer ?">Me contacter ?</p>
            <div id="formContact" class="modal">
                <div class="modal-content">
                    <h4>Me contacter</h4>
                </div>
                <form method="post" class="container" id='contact'>
                    <div class="input-field col s12">
                        <input id="prenom" name="prenom" type="text" maxlength="25" data-length="25" class="validate" title="Votre prénom" />
                        <label for="prenom" class="black-text">Prénom</label>
                        <span id="msg_prenom"></span>
                    </div>
                    <div class="input-field col s12">
                        <input id="sujet" name="sujet" type="text" maxlength="25" data-length="25" class="validate" title="Le sujet que vous souhaitez aborder" />
                        <label for="sujet" class="black-text">Sujet</label>
                        <span id="msg_sujet"></span>
                    </div>
                    <div class="input-field col s12">
                        <input id="mail" name="mail" type="email" maxlength="255" data-length="255" class="validate" title="Votre adresse mail" />
                        <label for="mail" class="black-text">Mail</label>
                        <span id="msg_mail"></span>
                    </div>
                    <div class="input-field col s12">
                        <textarea id="text" name="text" class="materialize-textarea" title="Message que vous souaitez envoyer"></textarea>
                        <label for="text" class="black-text">Quel message veux-tu m'envoyer ?</label>
                        <span id="msg_text"></span>
                    </div>
                    <input type="submit" id='btnContact' class="btn" />
                </form>
              <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fermer</a>
              </div>
            </div>
            <p><a href="https://www.facebook.com/WebLinefr/" target="_blank" title="Page facebook - WebLine"><img src="asset/frontOffice/img/face.png" class="responsive-img" alt="Icône - Facebook"></a></p>
        </footer>
        <script src="../bower_components/jquery/dist/jquery.min.js" type="text/javascript" defer></script>
        <script src="../bower_components/materialize/dist/js/materialize.min.js" type="text/javascript" defer></script>
        <script src="../asset/frontOffice/js/script.min.js" type="text/javascript" defer></script>
    </body>
</html>