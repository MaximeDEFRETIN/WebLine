<?php require_once 'header.php' ?>
    <div class="row">
        <a href="/" class="btn col s4 offset-s4 teal lighten-4 marginTopMax" title="Accueil">Accueil</a>
        <p class="center-align col s12 marginTop">Le serveur ne fonctionne plus correctement.</p>
        <p class="center-align col s12 marginTopMin">Tout est mis en oeuvre pour qu'il refonctionne correctement.</p>
        <p class="center-align col s12 marginTopMin">Revenez un peu plus tard, le problème sera réglé.</p>
    </div>
<?php require_once 'footer.php' ?>