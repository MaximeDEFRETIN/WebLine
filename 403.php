<?php require_once 'header.php' ?>
    <div class="row">
        <a href="/" class="btn col s4 offset-s4 teal lighten-4 marginTopMax" title="Accueil">Accueil</a>
        <p class="center-align col s12 marginTopMax">Tu ne peux pas accéder à la page demandée.</p>
        <p class="center-align col s12 marginTop">La page à laquelle tu cherches à te connecter ne t'es pas accessible car elle est limitée à un nombre restreint de personne.</p>
    </div>
<?php require_once 'footer.php' ?>