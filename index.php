<?php
require_once 'header.php';
?>
<div class="parallax-container">
    <div id="apAcc" class="container marginTopMax teal-text">
        <p><span id="numSearch"></span> C'est le nombre de français, de 16 à 74 ans, faisant toutes leurs recherches depuis leurs domiciles. Chaque internaute passe même, en moyenne, 16 heures sur internet.</p>
        <p><span id="numStatEC"></span>C'est le chiffre d'affaire, en milliard de dollar, généré par le e-commerce à travers le monde, en 2017. En 2016, le e-commerce permit de générer 1915 milliards de dollar de chiffre d'affaire. En 2015, le chiffre d'affaire fut de 1548 milliards de dollar.</p>
        <p class="center-align"><a href="https://www.blogdumoderateur.com/chiffres-e-commerce-2016-2017/" class="sourcesLien" title="Sources blogdumoderateur.com">blogdumoderateur.com</a> <a href="https://www.anthedesign.fr/communication-2/quel-est-linteret-dun-site-internet-pour-votre-entreprise/" class="sourcesLien" title="Sources anthedesign.fr">anthedesign.fr</a></p>
    </div>
    <div class="parallax"><img src="/asset/frontOffice/img/apAcc.jpeg" class="responsive-img" alt="Image de fond - Accueil" title="Image de fond - Accueil" /></div>
</div>
<div class="container marginTop center-align">
    <h1 class="resizeText">Internet est devenu un outil incontournable.</h1>
    <p>Avoir un site web, c'est vous permettre d'être visible internet et de vous faire connaitre. Posséder un site web, c'est aussi le moyen de faire valoir votre enterprise, votre association ou vos compétences. Faites-vous connaitre et gagner en nototriété.</p>
</div>
<div class="row marginTop">
    <div class="container">
        <p class="col s5 prAcc">Site vitrine</p>
        <p class="col s5 offset-s2 prAcc">Site e-commerce</p>
        <p class="col s5 prAcc">Blog</p>
        <p class="col s5 offset-s2 prAcc">C.V. numérique</p>
    </div>
    <p class="container center-align">Votre projet n'appartient qu'à vous. WebLine vous accompagne dans la création d'un site web personnalisé ! Et de la définition de vos besoins naîtra votre présence sur internet.</p>
</div>
<div class="container marginTop center-align">
    <p>Chaque prestation fournie comprend le développement du site web, le référencement (SEO) ainsi que la réservation de votre nom de domaine et la définition de votre charte graphique si vous n'en avez pas.</p>
    <p>Vous avez besoin d'un site web ? Il ne vous manque plus qu'à me contacter.</p>
</div>
<?php require_once 'footer.php'; ?>