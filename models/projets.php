<?php
class project_displayed extends dataBase {
    
    public $id = 0;
    public $project_user_id = 0;
    public $id_adsvfd_project_displayed = 0;
    public $title = '';
    public $description_project = '';
    public $project_owner = '';
    public $link = '';
    public $created_at = '';
    public $path_img1 = '';

    /**
     * Permet de se connecter à la base de donnée grâce à une instance PDO
     */
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Permet de récupérer le dernier projet.
     * @return type
     */
    public function getLastProject() {
        $requestLastGetProject = $this->db->prepare('SELECT `id`, `title`, `description_project`, `project_owner`, `link`, DATE_FORMAT(`created_at`, "%Y") AS `created_at` FROM `'.self::prefix.'project_displayed` ORDER BY `id` DESC LIMIT 1');
        
        // Si la méthode est exécutée
        if($requestLastGetProject->execute()) {
            // On envoie les résultats
            return $displayLastProject = $requestLastGetProject->fetchAll(PDO::FETCH_OBJ);
        }
    }

    /**
     * Permet d'afficher l'ensemble des projets
     */
    public function getProjects() {
        $requestGetProject = $this->db->prepare('SELECT `'.self::prefix.'project_displayed`.`id`, `title`, `description_project`, `project_owner`, `link`, DATE_FORMAT(`created_at`, "%Y") AS `created_at`, `path_img1` FROM `'.self::prefix.'project_displayed` INNER JOIN `'.self::prefix.'img_project` ON `'.self::prefix.'img_project`.`id_'.self::prefix.'project_displayed` = `'.self::prefix.'project_displayed`.`id` ORDER BY `id` DESC');
                
        // Si la méthode est exécutée
        if($requestGetProject->execute()) {
            // On envoie les résultats
            return $displayProject = $requestGetProject->fetchAll(PDO::FETCH_OBJ);
        }
    }

    /**
     * Permet d'afficher un projet
     */
    public function getOneProject($number) {
        $requestGetOneProject = $this->db->prepare('SELECT `'.self::prefix.'project_displayed`.`id`, `title`, `description_project`, `project_owner`, `link`, DATE_FORMAT(`created_at`, "%Y") AS `created_at`, `path_img1` FROM `'.self::prefix.'project_displayed` INNER JOIN `'.self::prefix.'img_project` ON `'.self::prefix.'img_project`.`id_'.self::prefix.'project_displayed` = `'.self::prefix.'project_displayed`.`id` WHERE `'.self::prefix.'project_displayed`.`id` = :id');
        $requestGetOneProject->bindValue(':id', $number, PDO::PARAM_INT);
        // Si la méthode est exécutée
        if($requestGetOneProject->execute()) {
            // On envoie les résultats
            return $displayOneProject = $requestGetOneProject->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    /**
     * Permet d'ajouter un projet
     */
    public function insertProject($title, $description_project, $project_owner, $link, $path_img) {
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            try {
                // On démarre la transaction, toujours mettre la table enfant avant
                // la table parente pour éviter les soucis de suppression
                $this->db->beginTransaction();
                
                $requestInsertProject = $this->db->prepare('INSERT INTO `adsvfd_project_displayed`(`project_user_id`, `title`, `description_project`, `project_owner`, `link`, `created_at`) VALUES (1, :title, :description_project, :project_owner, :link, CURRENT_DATE())');
                $requestInsertProject->bindValue(':title', $title,PDO::PARAM_STR);
                $requestInsertProject->bindValue(':description_project', $description_project, PDO::PARAM_STR);
                $requestInsertProject->bindValue(':project_owner', $project_owner, PDO::PARAM_STR);
                $requestInsertProject->bindValue(':link', $link, PDO::PARAM_STR);
                $requestInsertProject->execute();
                
                $requestInsertImgProject = $this->db->prepare('INSERT INTO `'.self::prefix.'img_project`(`path_img1`, `id_'.self::prefix.'project_displayed`, `id_'.self::prefix.'user`) VALUES (:path_img1, (SELECT `id` FROM `'.self::prefix.'project_displayed` ORDER BY `id` DESC LIMIT 1), 1)');
                $requestInsertImgProject->bindValue(':path_img1', $path_img, PDO::PARAM_STR);
                $requestInsertImgProject->execute();
                
                $insertFKey = $this->db->prepare('UPDATE `'.self::prefix.'project_displayed` SET `id_'.self::prefix.'img_project` = (SELECT `id` FROM `'.self::prefix.'img_project` ORDER BY `id` DESC LIMIT 1) WHERE `id` = (SELECT `id_'.self::prefix.'project_displayed` FROM `'.self::prefix.'img_project` ORDER BY `id` DESC LIMIT 1)');
                $insertFKey->execute();
                
                // commit() permet d'éxécuter les requêtes
                return $this->db->commit();
            } catch (Exception $ex) {
                // Si une erreur survient, on annule les changements.
                $this->db->rollBack();
                echo 'Erreur : ' . $ex->getMessage();
            }
    }
    
    /**
     * Permet de récupérer une image
     */
    public function getPathIMG($id) {
        $requestGetPathIMG = $this->db->prepare('SELECT `path_img1` FROM `'.self::prefix.'img_project` WHERE `id_'.self::prefix.'project_displayed` = :id');
        $requestGetPathIMG->bindValue(':id', $id, PDO::PARAM_INT); 
        if($requestGetPathIMG->execute()) {
            if (is_object($requestGetPathIMG)) {
                return $displayPathIMG = $requestGetPathIMG->fetchAll(PDO::FETCH_OBJ);
            }
        }
    }


    /**
     * Permet de mettre à jour un projet
     */
    public function updateProject($project_user_id, $title, $description_project, $project_owner, $link, $id, $path_img1) {
            try {
                // On démarre la transaction, toujours mettre la table enfant avant
                // la table parente pour éviter les soucis de suppression
                $this->db->beginTransaction();
                $requestUpdateProject = $this->db->prepare('UPDATE `'.self::prefix.'project_displayed` SET `project_user_id`= :project_user_id, `title` = :title, `description_project` = :description_project, `project_owner` = :project_owner, `link` = :link WHERE `id` = :id');
                $requestUpdateProject->bindValue(':project_user_id', $project_user_id, PDO::PARAM_INT);
                $requestUpdateProject->bindValue(':title', $title,PDO::PARAM_STR);
                $requestUpdateProject->bindValue(':description_project', $description_project, PDO::PARAM_STR);
                $requestUpdateProject->bindValue(':project_owner', $project_owner, PDO::PARAM_STR);
                $requestUpdateProject->bindValue(':link', $link, PDO::PARAM_STR);
                $requestUpdateProject->bindValue(':id', $id, PDO::PARAM_INT);
                $requestUpdateProject->execute();
                
                $requestUpdateImg = $this->db->prepare('UPDATE `'.self::prefix.'img_project` SET `path_img1` = :path_img1 WHERE `id` = :id');
                $requestUpdateImg->bindValue(':path_img1', $path_img1, PDO::PARAM_STR);
                $requestUpdateImg->bindValue(':id', $id, PDO::PARAM_INT);
                $requestUpdateImg->execute();
                
                // commit() permet d'éxécuter les requêtes
                return $this->db->commit();
            } catch (Exception $ex) {
                // Si une erreur survient, on annule les changements.
                $this->db->rollBack();
                echo 'Erreur : ' . $ex->getMessage();
            }
    }
    
    /**
     * Permet de supprimer un projet
     */
    public function deleteProject($id) {
        $requestDeleteProject = $this->db->prepare('DELETE FROM `'.self::prefix.'project_displayed` WHERE `id` = :id');
        $requestDeleteProject->bindValue(':id', $id, PDO::PARAM_INT);
        
        return $requestDeleteProject->execute();
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}