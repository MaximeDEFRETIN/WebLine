<?php
class opinion extends dataBase {
    
    public $id = 0;
    public $opinion_user_id = 0;
    public $author = '';
    public $created_at = '';
    public $opinion = '';
    public $note = '';
    public $mail = '';
    public $validate = 0;
    
    /**
     * Permet de se connecter à la base de donnée grâce à une instance PDO
     */
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Permet d'afficher l'ensemble des opinions
     */
    public function getOpinion($validate, $order) {
        $requestGetOpinion = $this->db->prepare('SELECT `id`, `author`,  DATE_FORMAT(`created_at`, "%d/%m/%Y") AS `created_at`, `opinion`, `note` FROM `'.self::prefix.'opinion` WHERE `validate` = :validate ORDER BY `id` ' . $order);
        $requestGetOpinion->bindValue(':validate', $validate, PDO::PARAM_INT);
        if ($requestGetOpinion->execute()) {
            return $display = $requestGetOpinion->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    /**
     * Permet d'ajouter une opinion
     */
    public function insertOpinion($author, $opinion, $note) {
        $requestInsert = $this->db->prepare('INSERT INTO `adsvfd_opinion`(`opinion_user_id`, `author`, `created_at`, `opinion`, `note`, `validate`) VALUES (1, :author, CURRENT_DATE(), :opinion, :note, 0)');
        $requestInsert->bindValue(':author', $author, PDO::PARAM_STR);
        $requestInsert->bindValue(':opinion', $opinion, PDO::PARAM_STR);
        $requestInsert->bindValue(':note', $note, PDO::PARAM_INT);
        return $requestInsert->execute();
    }
    
    /**
     * Permet de récupérer une adresse mail en fonction d'un id
     */
    public function getMailById($id) {
        $requestGetMail = $this->db->prepare('SELECT `mail` FROM `'.self::prefix.'opinion` WHERE `id` = :id');
        $requestGetMail->bindValue(':id', $id, PDO::PARAM_INT);
        
        // Si la méthode est exécutée
        if($requestGetMail->execute()) {
            // On envoie les résultats
            return $getMail = $requestGetMail->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    /**
     * Permet de valider une opinion
     */
    public function updateOpinion($validate, $id) {
        $requestUpdateOpinion = $this->db->prepare('UPDATE `'.self::prefix.'opinion` SET `validate`= :validate WHERE `id` = :id');
        $requestUpdateOpinion->bindValue(':validate', $validate, PDO::PARAM_INT);
        $requestUpdateOpinion->bindValue(':id', $id, PDO::PARAM_INT);
        return $requestUpdateOpinion->execute();
    }
    
    /**
     * Permet de supprimer une opinion
     */
    public function deleteOpinion($id) {
        $requestDeleteOpinion = $this->db->prepare('DELETE FROM `' . self::prefix . 'opinion` WHERE `id` = :id');
        $requestDeleteOpinion->bindValue(':id', $id, PDO::PARAM_INT);
        return $requestDeleteOpinion->execute();
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}