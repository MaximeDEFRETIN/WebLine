<?php
class user extends dataBase {
    
    public $first_name = '';
    public $last_name = '';
    public $mail = '';
    public $number_phone = 0000000000;
    public $birthdate_at = '';
    public $location = '';
    public $status = '';
    public $password = '';
    public $username = '';
    public $description = '';
    
    /**
     * Permet de se connecter à la base de donnée grâce à une instance PDO
     */
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Permet de récupérer les informations d'un utilisateur
     */
    public function getUser($mail) {
        $requestUser = $this->db->prepare('SELECT `id`, `first_name`, `last_name`, `mail`, `number_phone`, `birthdate_at`, `location`, `status`, `password`, `username`, `description` FROM `'.self::prefix.'user` WHERE `mail` = :mail');
        $requestUser->bindValue(':mail', $mail, PDO::PARAM_STR);
        
        // Si la méthode est exécutée
        if($requestUser->execute()) {
            // On envoie les résultats
            return $displayUser = $requestUser->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    /**
     * Permet de récupérer l'adresse mail et le mot de passe de l'utilisateur pour le connecter
     */
    public function getUserByMail($mail) {
        $exists = false;
        $requestUser = $this->db->prepare('SELECT `id`, `first_name`, `last_name`, `mail`, `number_phone`, `birthdate_at`, `location`, `status`, `password`, `username`, `description` FROM `'.self::prefix.'user` WHERE `mail` =  :mail');
        $requestUser->bindValue(':mail', $mail, PDO::PARAM_STR);
        if ($requestUser->execute()) {
            if (is_object($requestUser)) {
                if($queryUserResult = $requestUser->fetch(PDO::FETCH_OBJ)) {
                    $this->password = $queryUserResult->password;
                    $this->id = $queryUserResult->id;
                    $this->last_name = $queryUserResult->last_name;
                    $this->first_name = $queryUserResult->first_name;
                    $this->number_phone = $queryUserResult->number_phone;
                    $this->birthdate_at = $queryUserResult->birthdate_at;
                    $this->location = $queryUserResult->location;
                    $this->mail = $queryUserResult->mail;
                    $this->username = $queryUserResult->username;
                    $this->description = $queryUserResult->description;
                    return $exists = true;
                }
            }
        }
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}