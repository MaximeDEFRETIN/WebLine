<?php
session_start();

require_once '../models/dataBase.php';
require_once '../models/projets.php';
require_once '../models/opinions.php';
require_once '../models/user.php';

(empty($_SESSION))?header('Location: 403'):'';
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device=width, initial-scale=1.0" />
        <meta name="robots" content="noindex" />
        <link rel="stylesheet" href="../bower_components/materialize/dist/css/materialize.min.css" />
        <script src="../bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../asset/css/style.css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>WebLine - Tableau de bord</title>
    </head>
    <body class="row">
        <header>
            <a href="../controllers/frontOffice/deconnexion.php">Déconnexion</a>
        </header>