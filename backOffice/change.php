<?php
require_once 'header.php';
require_once '../controllers/backOffice/change-backOffice.php';
?>
    <h1 class="center-align">Modifications</h1>
    <?php
    $messageUpdate = array($messageUpdateProject);
    foreach ($messageUpdate as $message) { ?>
        <p class='center-align'><?= implode($message) ?></p>
    <?php }
          if (isset($_GET['chPr']) && isset($_SESSION['id'])) {
    foreach ($projetBackOfficeChange as $value) { ?>
        <form method="POST" enctype="multipart/form-data" class="row">
            <div class="input-field col offset-s3 s6">
                <input class="validate center-align" id="titleUp" type="text" name="titleUp" minlength="1" maxlength="25" required value="<?= $value->title ?>" />
                <label for="titleUp" class="validate black-text">Titre</label>
            </div>
            <div class="input-field col offset-s3 s6">
                <textarea class="materialize-textarea center-align" id="descriptionUp" type="text" name="descriptionUp" minlength="1" maxlength="500" ><?= $value->description_project ?></textarea>
                <label for="descriptionUp" class="validate black-text">Description</label>
            </div>
            <div class="input-field col offset-s3 s6">
                <input class="validate center-align" id="proprietaireUp" type="text" name="propretaireUp" minlength="1" maxlength="50" required value="<?= $value->project_owner ?>" />
                <label for="proprietaireUp" class="validate black-text">Propriétaire</label>
            </div>
            <div class="input-field col offset-s3 s6">
                <input class="validate center-align" id="Up" type="url" name="linkUp" minlength="1" maxlength="255" required value="<?= $value->link ?>" />
                <label for="linkUp" class="validate black-text">Lien</label>
            </div>
            <div class="input-field col offset-s3 s6">
                <input type="text" name="pi1" id="pi1" value="<?= $value->path_img1 ?>" readonly />
                <label for="pi1" class="validate black-text">Nom du fichier</label>
                <img src="/asset/global/img/projets/<?= $value->path_img1 ?>" />
            </div>
                <div class="col s4 offset-s4 input-field file-field">
                    <p class="center-align">Image</p>
                    <input type="file" name="chimg" accept=".jpeg" required />
                    <div class="file-path-wrapper">
                        <input class="file-path validate center-align" name="chtxt" type="text" />
                    </div>
                </div>
            <input class="btn col s4 offset-s4" type="submit" name="submitUp" />
        </form>
    <?php } } ?>
    <a href="../Interface" class="btn col s4 offset-s4">Retour</a>
<?php require_once '../backOffice/footer.php'; ?>