<?php
require_once '../backOffice/header.php';
require_once '../controllers/backOffice/display-backOffice.php';
?>
<h1 class="center-align">Interfaces</h1>
<h3 class="center-align">Statistiques</h3>
<?php foreach($messageInsert as $display) { ?>
    <p class="center-align"><?= $display ?></p>
<?php } ?>
<div class="col s12 center-align">
    <div class="col s6">
        <div class="col s12">
            <h4 class="center-align">Opinions non validées</h4>
            <button class="btn" id="moreOpinionsNo" onclick="more()">Afficher le reste</button>
            <table class="responsive-table centered highlight">
                <thead>
                    <tr>
                        <th>Auteur(e)</th>
                        <th>Opinion</th>
                        <th>Note</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody id="listeOpinionsNo">
                    <?php foreach ($opinionBackOfficeDisplayNo as $display) { ?>
                        <tr>
                            <td><?= $display->author?></td>
                            <td><?= $display->opinion ?></td>
                            <td><?= $display->note ?></td>
                            <td datetime="<?= $display->created_at ?>"><?= $display->created_at ?></td>
                            <td><a href="../backOffice/index.php?vaOp=<?= $display->id ?>"><i class="small material-icons" title="Non validé">priority_high</i></a></td>
                            <td><a href="../backOffice/index.php?delOp=<?= $display->id ?>"><i class="small material-icons" title="Supprimer">close</i></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col s12">
        <h4 class="center-align">Opinions validées</h4>
        <button class="btn" id="moreOpinionsVa">Afficher le reste</button>
        <table class="responsive-table centered highlight">
            <thead>
                <tr>
                    <th>Auteur(e)</th>
                    <th>Opinion</th>
                    <th>Note</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody id="listeOpinionsVa">
                <?php foreach ($opinionBackOfficeDisplay as $display) { ?>
                    <tr>
                        <td><?= $display->author?></td>
                        <td><?= $display->opinion ?></td>
                        <td><?= $display->note ?></td>
                        <td datetime="<?= $display->created_at ?>"><?= $display->created_at ?></td>
                        <td><a href="../backOffice/index.php?delOp=<?= $display->id ?>"><i class="small material-icons" title="Supprimer">close</i></a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        </div>
    </div>
    <div class="col s6">
        <h4 class="center-align">Projets</h4>
        <div class="col s12">
            <button data-target="formProject" class="btn modal-trigger col s10 offset-s1 marginTopMin" title="Ajouter un projet">Ajouter un projet</button>
            <div id="formProject" class="modal">
                <div class="modal-content">
                    <h2 class="center-align">Ajouter un projet</h2>
                    <form method="POST" enctype="multipart/form-data">
                        <div class="col s12 input-field">
                            <input type="text" name="title" id="title" maxlength="25" data-length="25" title="Titre" />
                            <label for="title" class="black-text">Titre</label>
                        </div>
                        <div class="col s12 input-field">
                            <input type="text" name="project_owner" id="project_owner" maxlength="25" data-length="25" title="Titre" />
                            <label for="project_owner" class="black-text">Propriétaire</label>
                        </div>
                        <div class="marginTop col s12 input-field">
                            <textarea class="materialize-textarea" type="text" name="description_project" id="description_project" maxlength="255" data-length="255" title="Avis"></textarea>
                            <label for="description_project" class="black-text">Description du projet</label>
                        </div>
                        <div class="col s12 input-field file-field">
                            <input type="url" name="link" id="link" class="validate" maxlength="255" data-length="255" title="URL" />
                            <label for="link" class="black-text">URL</label>
                        </div>
                        <div class="col s4 offset-s4 input-field file-field">
                            <p class="center-align">Image</p>
                            <input type="file" name="img" accept=".jpeg" required />
                            <div class="file-path-wrapper">
                                <input class="file-path validate center-align" type="text" />
                            </div>
                        </div>
                        <input type="submit" name="projectSubmit" class="btn col s6 offset-s3 marginBottomMin marginTopMin" value="Enregistre" />
                    </form>
                </div>
            </div>
        </div>
        <button class="btn marginTopMin" id='moreProjets'>Afficher le reste</button>
        <table class="responsive-table centered highlight">
            <thead>
                <tr>
                    <th>Titre</th>
                    <th>Description</th>
                    <th>Propriétaire</th>
                    <th>Lien</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody class="liste" id="listeProjets">
                <?php foreach ($projetsBackOfficeDisplay as $display) { ?>
                    <tr>
                        <td><?= $display->title ?></td>
                        <td><?= $display->description_project ?></td>
                        <td><?= $display->project_owner ?></td>
                        <td><a href="<?= $display->link ?>"><?= $display->link ?></a></td>
                        <td datetime="<?= $display->created_at ?>"><?= $display->created_at ?></td>
                        <td><a href="../Changement-projet-<?= $display->id ?>"><i class="small material-icons" title="Modifier">edit</i></a></td>
                        <td><a href="../backOffice/index.php?delPr=<?= $display->id ?>&path=<?= $display->path_img1 ?>"><i class="small material-icons" title="Supprimer">close</i></a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php require_once '../backOffice/footer.php'; ?>