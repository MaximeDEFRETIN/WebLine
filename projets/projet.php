<?php
require_once '../header.php';
require_once '../controllers/frontOffice/displays.php';
?>
<article class="marginTopMax">
    <?php foreach ($projetDisplay as $display) { ?>
        <div class="col s12 valign-wrapper">
            <div class="col offset-s1 s10">
                <h1 class="center-align"><?= $display->title ?></h1>
                <div class="row center-align">
                    <p class="col s6">Propriétaire : <?= $display->project_owner ?></p>
                    <p class="col s6">Réalisé en <em><time datetime="<?= $display->created_at ?>"><?= $display->created_at ?></em></time></p>
                    <a class="col s12" href="<?= $display->link ?>"><?= basename($display->link) ?></a>
                </div>
                <img src="/asset/global/img/projets/<?= $display->path_img1 ?>" />
            </div>
        </div>
        <div class="col offset-s1 s10 justify">
            <p><?= $display->description_project ?></p>
        </div>
    <?php } ?>
</article>
<?php require_once '../footer.php'; ?>