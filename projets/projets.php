<?php
require_once '../header.php';
require_once '../controllers/frontOffice/displays.php';
?>
<section class="col s10 offset-s1 marginTopMax center-align">
    <h1 class="resizeText">Voici la liste de tout les projets que j'ai réalisé</h1>
    <?php foreach ($projetsDisplay as $display) { ?>
        <article class="projets col l3 m3 offset-l2 offset-m2 margintBottomMin">
            <h2 class="col s12 resizeTextMin"><?= $display->title ?></h2>
            <div class="col s12">
                <p>Appartient à <?= $display->project_owner ?></p>
                <p>Réalisé en <em><time datetime="<?= $display->created_at ?>"><?= $display->created_at ?></em></time></p>
            </div>
            <a class="col s12" href="/Projet-<?= $display->id ?>" title="<?= $display->title ?>">Lire la suite</a>
        </article>
    <?php } ?>
</section>
<?php require_once '../footer.php'; ?>