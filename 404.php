<?php require_once 'header.php' ?>
    <div class="row">
        <a href="/" class="btn col s4 offset-s4 teal lighten-4 marginTopMax" title="Accueil">Accueil</a>
        <p class="center-align col s12 marginTopMax">La page que tu recherches n'existe pas.</p>
        <p class="center-align col s12 marginTop">Soit elle a été supprimé, soit elle n'est pas actuellement disponible.</p>
    </div>
<?php require_once 'footer.php' ?>